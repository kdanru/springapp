package com.mgd.mgdApp.controllers;


import com.mgd.mgdApp.ActorTable;
import com.mgd.mgdApp.FilmTable;
import com.mgd.mgdApp.repos.ActorRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class ActorController {

    @Autowired
    private ActorRepo actorRepo;

    @GetMapping("/actors")
    public  String main(Map<String, Object> model){
        Iterable<ActorTable> all = actorRepo.findAll();

        model.put("actors",all);
        return  "main";
    }

    @PostMapping("/actors/adding")
    public String add(@RequestParam String inputFirstName,
                      @RequestParam String  inputLastName,
                      Map<String, Object> model){
        ActorTable actorTable = new ActorTable(inputFirstName, inputLastName);

        actorRepo.save(actorTable);

        Iterable<ActorTable> all = actorRepo.findAll();
        model.put("actors",all);

        return "main";
    }

    @PostMapping("/actors/delete")
    public String delete(@RequestParam Integer inputActorId, Map<String, Object> model){
        try{
            actorRepo.deleteById(inputActorId);
        }catch (Exception e){

        }
        Iterable<ActorTable> deleteResult= actorRepo.findAll();
        model.put("actors", deleteResult);
        return "main";
    }

}
