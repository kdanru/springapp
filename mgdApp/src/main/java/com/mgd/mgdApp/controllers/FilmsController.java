package com.mgd.mgdApp.controllers;


import com.mgd.mgdApp.FilmTable;
import com.mgd.mgdApp.repos.FilmsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@RestController
@RequestMapping("/api")
public class FilmsController {
    @Autowired
    private FilmsRepo filmsRepo;


    @GetMapping("/films")
    public ModelAndView main(Map<String, Object> model){
        Iterable<FilmTable> all = filmsRepo.findAll();

        model.put("films",all);
        return new ModelAndView("main",model);
    }


    @PostMapping("/films/adding")
    public String add(@RequestParam String inputTitle,
                      @RequestParam String  inputDescription,
                      @RequestParam Integer inputReleaseYear,
                      @RequestParam Integer inputLength,
                      @RequestParam Double inputReplacementCost,
                      @RequestParam String inputRating,
                      Map<String, Object> model){
        FilmTable filmTable = new FilmTable(inputTitle, inputDescription, inputReleaseYear, inputLength, inputReplacementCost, inputRating);

        filmsRepo.save(filmTable);

        Iterable<FilmTable> all = filmsRepo.findAll();
        model.put("films",all);

        return "main";
    }



    @PostMapping("/films/filter")
    public String filter(@RequestParam String inputFilter, Map<String, Object> model){
        Iterable<FilmTable> filterResult;
        if(inputFilter!=null&&!inputFilter.isEmpty()){
            filterResult = filmsRepo.findByTitle(inputFilter);
        }
        else{
            filterResult = filmsRepo.findAll();
        }
        model.put("filterres", filterResult);
        return "main";
    }

    @PostMapping("/films/delete")
    public String delete(@RequestParam Integer inputFilmId, Map<String, Object> model){
        try{
        filmsRepo.deleteById(inputFilmId);}catch (Exception e){

        }
        Iterable<FilmTable> filterResult= filmsRepo.findAll();
        model.put("films", filterResult);
        return "main";
    }

}


