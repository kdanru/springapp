package com.mgd.mgdApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MgdAppApplication {

	public static void main(String[] args) {


		SpringApplication.run(MgdAppApplication.class, args);


	}
}
