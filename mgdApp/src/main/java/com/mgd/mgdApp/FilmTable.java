package com.mgd.mgdApp;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.Null;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

@Entity
@Table (name="film")
public class FilmTable {
    @Id
    @Column(name="film_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer filmId;
    @Column(name="title")
    private String title;
    @Column(name= "description")
    private String description;
    @Column(name = "releaseYear")
    private Integer releaseYear;
    @Column(name = "language_id")
    private Integer languageId;
    //Некорректно работает связь с таблицей
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "language_id", insertable = false, updatable = false)
    @JsonIgnoreProperties("films")
    private LanguageTable languageTable;

    @Column(name="rental_duration")
    private Integer rentalDuration =null;
    @Column(name="rental_rate")
    private Integer rentalRate = null;



    @Column(name="length")
    private Integer length;
    @Column(name="replacement_cost")
    private Double replacementCost;
    @Column(name = "rating")
    private String rating;
    @Column(name="last_update")
    private Date lastUpdate;
    @Column(name="special_features")
    private String specialFeatures;
    @Column(name = "full_text")
    private String fulltext;

    public FilmTable() {
    }

    public FilmTable(String title, String description, Integer releaseYear, Integer length, Double replacementCost, String rating) {
        this.title = title;
        this.description = description;
        this.releaseYear = releaseYear;
        this.length = length;
        this.replacementCost = replacementCost;
        this.rating = rating;
        this.specialFeatures = specialFeatures;
        this.rentalDuration = null;
        this.rentalRate = null;
        this.lastUpdate = new Date();
        this.specialFeatures = null;

    }

    /*public Integer getLanguageId() {
        if(languageId==null){
            return 1;
        } else{
            return languageId;
        }
    }*/

    public void setFilmId(Integer filmId) {
        this.filmId = filmId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    public void setLangId(Integer langId) {
        this.languageId = langId;
    }



    public void setLength(int length) {
        this.length = length;
    }

    public void setReplacementCost(Double replacementCost) {
        this.replacementCost = replacementCost;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setSpecialFeatures(String specialFeatures) {
        this.specialFeatures = specialFeatures;
    }

    public int getFilmId() {
        if(filmId == null)
            return 0;
        else
            return filmId;
    }

    public String getSpecialFeatures() {
        if(specialFeatures ==null)
            return "";
        else
            return specialFeatures;
    }

    /*public LanguageTable getLanguageTable() {
        if (languageTable == null) {
            return new LanguageTable();
        } else {
            return languageTable;
        }
    }

    public void setLanguageTable(LanguageTable languageTable) {
        this.languageTable = languageTable;
    }
*/
    public String getRating() {
        return rating;
    }

    public Double getReplacementCost() {
        return replacementCost;
    }

    public int getLength() {
        return length;
    }


    public Integer getReleaseYear() {
        return releaseYear;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }
}
