package com.mgd.mgdApp.repos;

import com.mgd.mgdApp.FilmTable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface FilmsRepo extends CrudRepository<FilmTable, Integer> {

    List<FilmTable> findByTitle(String title);
}
