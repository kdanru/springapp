package com.mgd.mgdApp.repos;

import com.mgd.mgdApp.FilmCategoryTable;
import org.springframework.data.repository.CrudRepository;

public interface FilmCategoryRepo extends CrudRepository<FilmCategoryTable, Integer> {
}
