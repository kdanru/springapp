package com.mgd.mgdApp;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "language")
public class LanguageTable {
    @Id
    @Column(name = "language_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  Integer languageId;

    @Column (name = "last_update", nullable = false)
    private Date lastUpdate;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "languageTable", fetch = FetchType.LAZY)
    @JsonIgnoreProperties("languageTable")
    private Set<FilmTable> films = new HashSet<>();

    public LanguageTable() {
    }

    public LanguageTable(Date lastUpdate, String name) {
        this.lastUpdate = lastUpdate;
        this.name = name;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<FilmTable> getFilms() {
        return films;
    }

    public void setFilms(Set<FilmTable> films) {
        this.films = films;
    }
}
